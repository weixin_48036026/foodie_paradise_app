const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  transpileDependencies: true,
    // 简单配置一下
    lintOnSave: true, // 规范写法，如组件名称驼峰名组件，使用组件用小写和“-”隔开两个单词
    publicPath: './',
    // 启动服务自动打开浏览器
    devServer: {
      open: true
    }
})
