import Vue from 'vue'
import App from './App.vue'
import router from './router'

// 导入做布局适配模块
import 'amfe-flexible'

import './assets/css/reset.css'

// 手动按需引入组件
import './common/comp/index'

Vue.config.productionTip = false


new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
