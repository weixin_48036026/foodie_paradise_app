import Vue from 'vue'
import VueRouter from 'vue-router'
import LayoutView from '../LayoutView.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    redirect: '/index/recommend'
  },
  {
    path: '/index/recommend',
    name: 'home',
    component: LayoutView,
    // 一级导航
    children: [
      {
        path: '/index',
        name: 'home',
        component: () => import('../views/HomeView.vue'),
        // 二级导航
        children: [
          {
            path: 'attention',
            name: 'attention',
            component: () => import('../views/page/attention.vue')
          },
          {
            path: 'recommend',
            name: 'recommend',
            component: () => import('../views/page/recommend.vue')
          },
          {
            path: 'notes',
            name: 'notes',
            component: () => import('../views/page/notes.vue')
          },
          {
            path: 'video',
            name: 'video',
            component: () => import('../views/page/video.vue')
          },
          {
            path: 'food',
            name: 'food',
            component: () => import('../views/page/food.vue')
          },
          {
            path: 'activities',
            name: 'activities',
            component: () => import('../views/page/activities.vue')
          },
        ]
      },
      {
        path: '/classroom',
        name: 'classroom',
        component: () => import('../views/ClassroomView.vue')
      },
      {
        path: '/shopping',
        name: 'shopping',
        component: () => import('../views/ShoppingView.vue')
      },
      {
        path: '/favorites',
        name: 'favorites',
        component: () => import('../views/FavoritesView.vue')
      },

      {
        path: '/my',
        name: 'my',
        component: () => import('../views/MyView.vue')
      },
    ]
  },

  {
    path: '/search',
    name: 'search',
    component: () => import('../views/SearchView.vue'),
  },
  {
    path: '/released',
    name: 'released',
    component: () => import('../views/ReleasedView.vue'),
  },
  {
    path: `/detail/:id`,
    name: 'detail',
    component: () => import('../views/DetailView.vue'),
  },

  {
    path: '/login',
    name: 'login',
    component: () => import('../views/LoginView.vue'),
  },
  {
    path: '/register',
    name: 'register',
    component: () => import('../views/RegisterView.vue'),
  },
  {
    path: '/classification',
    name: 'classification',
    component: () => import('../views/ClassificationView.vue'),
  },
  {
    path: '/logout',
    name: 'logout',
    component: () => import('../views/LogOutView.vue'),
  },
  {
    path: '*',
    component: () => import('../views/ErrorView.vue')
  },
]

const router = new VueRouter({
  routes
})

export default router
