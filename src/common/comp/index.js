import Vue from 'vue'
// 1） 导入vant样式文件
import 'vant/lib/index.css';

// 导入按钮组件样式
// import 'vant/lib/button/style';

// 2） 导入组件
import { 
    Button,
    Icon,
    Empty,
    Dialog,
    Form,
    Checkbox,
    Field,
    Toast,
    Swipe,
    SwipeItem,
    Search,
    Loading

} from 'vant';

// 3） 使用vant组件
Vue.use(Button);
Vue.use(Icon);
Vue.use(Empty);
Vue.use(Dialog);
Vue.use(Form);
Vue.use(Checkbox);
Vue.use(Field);
Vue.use(Toast);
Vue.use(Swipe);
Vue.use(SwipeItem);
Vue.use(Search);
Vue.use(Loading);

