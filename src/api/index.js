// 导入axios实例
import http from './request';


// http.get()   可以返回promise实例

// 接口1：推荐列表
export const recommend = (data={})=> {
    return http.get('/home/recommended/20/10',{params: data})
}

// 接口2：关注列表
export const attention = (data={})=> {
    return http.get('/home/ffeeds/0/20',{params: data})
}

// 接口3：笔记列表
export const notes = (data={})=> {
    return http.get('/home/notes/0/20',{params: data})
}

// // 接口4：视频列表
// export const video = (data={})=> {
//     return http.get('/personalized/newsong',{params: data})
// }

// // 接口5： 活动列表
// export const activities = (data={})=> {
//     return http.get('/lyric',{params: data})
// }


// 接口6： 详情页
// export const activities = (data={})=> {
//     return http.get('/recipe/detail',{params: data})
// }

// 接口7：热门搜索
export const hotSearch = ()=> {
    return http.get('/recipe/search/hot')
}